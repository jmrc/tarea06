# Tarea 6. Índice

  1. Explicación Docker
  2. Instalación
  2.1. Linux
  2.2. Windows
  3. Imágenes
  4. Contenedores
  4.1. Ubuntu
  4.2. MongoDB - RoboMongo
  5. Dockerfile
  5.1. PostgreSQL - PGAdmin
  6. Docker Compose
  6.1. WordPress
  6.2. LAMP
 
